LFLAGS=`sdl2-config --libs` -lSDL2_net -lSDL2_image -lSDL2_ttf
CFLAGS=-Wall -Wextra -std=c11 `sdl2-config --cflags`
TARGET=app

main: event-queue.o base.o net.o ui.o main.o
	cc $(LFLAGS) -o$(TARGET) $^

clean:
	rm -f *.o

event-queue.o: src/event-queue.c src/event-queue.h
	cc $(CFLAGS) -o$@ -c $<

base.o: src/base.c src/base.h src/event-queue.h
	cc $(CFLAGS) -o$@ -c $<

net.o: src/net.c src/net.h src/net/*.c src/base.h
	cc $(CFLAGS) -o$@ -c $<

ui.o: src/ui.c src/ui.h src/ui/*.c src/base.h
	cc $(CFLAGS) -o$@ -c $<

main.o: src/main.c src/base.h src/net.h
	cc $(CFLAGS) -o$@ -c $<

