#include <stdio.h>

#define HINT_LEN 30
#define ANSWER_LEN 62

struct {
	char hints[4][HINT_LEN + 1];
	char answer[ANSWER_LEN];
} hint_qs[2][6] = {{
	{
		.hints = {"First hint", "Second hint", "Third hint", "Fourth hint"},
		.answer = "This is how hints are numbered"
	},
	{
		.hints = {"B1", "B2", "B3", "B4"},
		.answer = "Fictional routes"
	},
	{
		.hints = {"C1", "C2", "C3", "C4"},
		.answer = "Fictional bombs"
	},
	{
		.hints = {"d1", "d2", "d3", "d4"},
		.answer = "Squares on a chess board"
	},
	{
		.hints = {"lorem", "ipsum", "lorem", "ipsum"},
		.answer = "lorem ipsum"
	},
	{
		.hints = {"АБ", "ВГ", "ҐД", "ЕЄ"},
		.answer = "Українська абетка"
	},
}, {
	{
		.hints = {"First hint", "Second hint", "Third hint", "Fourth hint"},
		.answer = "This is how hints are numbered"
	},
	{
		.hints = {"B1", "B2", "B3", "B4"},
		.answer = "Fictional routes"
	},
	{
		.hints = {"C1", "C2", "C3", "C4"},
		.answer = "Fictional bombs"
	},
	{
		.hints = {"d1", "d2", "d3", "d4"},
		.answer = "Squares on a chess board"
	},
	{
		.hints = {"lorem", "ipsum", "lorem", "ipsum"},
		.answer = "lorem ipsum"
	},
	{
		.hints = {"АБ", "ВГ", "ҐД", "ЕЄ"},
		.answer = "Українська абетка"
	},
}};

int main(void) {
	FILE *f = fopen("pack.zvz", "wb");
	for (int round = 0; round < 2; ++round) {
		for (int i = 0; i < 6; ++i) {
			for (int j = 0; j < 4; ++j) {
				fwrite(hint_qs[round][i].hints[j], sizeof(char), HINT_LEN + 1, f);
			}
			fwrite(hint_qs[round][i].answer, sizeof(char), ANSWER_LEN + 1, f);
		}
	}
	fclose(f);
}

