#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "base.h"

char log_msg[LOG_MSG_LEN]; // the full message (with no trailing newline) getting logged is also stored into this variable so other modules can access it directly if they so wish
SDL_mutex *log_mtx; // used inside `log` so nothing logs at the same time, should also be used when accessing `log_msg` from elsewhere

void flogf(FILE *file, const char *format, ...) {
	SDL_LockMutex(log_mtx);
	va_list va;
	va_start(va, format);
	vsnprintf(log_msg, LOG_MSG_LEN + 1, format, va); // XXX errors not handled
	va_end(va);
	fputs(log_msg, file);
	fputs("\n", file);
	SDL_UnlockMutex(log_mtx);
}

SDL_mutex *game_state_mtx;
struct game_state *game;
void lock_game_state(void) {
	SDL_LockMutex(game_state_mtx);
}
void unlock_game_state(void) {
	SDL_UnlockMutex(game_state_mtx);
}

int load_pack_file(void) {
	FILE *f;
	if (!(f = fopen("pack/pack.zvz", "rb"))) {
		log_err("failed to open file");
		return 2;
	}
	for (int r = 0; r < 2; ++r) {
		for (int i = 0; i < 6; ++i) {
			for (int j = 0; j < 4; ++j) {
				if (fread(game->questions[r][i].hints[j], 1, HINT_LEN + 1, f) != HINT_LEN + 1) {
					goto reading_error;
				}
			}
			if (fread(game->questions[r][i].answer, 1, ANSWER_LEN + 1, f) != ANSWER_LEN + 1) {
				goto reading_error;
			}
		}
	}
	fclose(f);
	return 0;
reading_error:
	log_err("reading error");
	return 1;
}

int init_game_state(struct game_state *game) {
	game->scene = SCENE_QUESTIONS;
	game->score[TEAM_BLUE] = game->score[TEAM_RED] = 0;
	game->round = 0;
	game->current_question = -1;
	game->current_hint_amt = 0;
	game->turn_team = TEAM_BLUE;
	if (is_host && load_pack_file()) {
		log_err("failed to load the pack");
		return 2;
	}
	for (int r = 0; r < 2; ++r) {
		for (int i = 0; i < 6; ++i) {
			game->questions[r][i].is_played = 0;
		}
	}
	strcpy(game->team_name[TEAM_BLUE], "TEAM BLUE");
	strcpy(game->team_name[TEAM_RED], "TEAM RED");
	game->hints_start_time = 0;
	return 0;
}

int push_team_name(const int team, const char c) {
	if (team != TEAM_BLUE && team != TEAM_RED) {
		log_wtf("invalid team specified (%d)", team);
		return 1;
	}
	const int len = strlen(game->team_name[team]);
	int rc = 1;
	if (len < TEAM_NAME_LEN) {
		game->team_name[team][len + 1] = 0;
		game->team_name[team][len] = c;
		rc = 0;
	}
	return rc;
}
int pop_team_name(const int team) {
	if (team != TEAM_BLUE && team != TEAM_RED) {
		log_wtf("invalid team specified (%d)", team);
		return 1;
	}
	const int len = strlen(game->team_name[team]);
	int rc = 1;
	if (len) game->team_name[team][len - 1] = 0, rc = 0;
	return rc;
}

int select_question(const int idx) {
	if (idx < 0 || idx > 5) {
		log_err("invalid question selected (%d)", idx);
		return 2;
	}
	if (game->round < 0 || game->round > 1) {
		log_err("wrong round to select a question (%d)", game->round);
		return 2;
	}
	if (game->scene != SCENE_QUESTIONS) {
		log_err("scene mismatch (%d)", game->scene);
		return 2;
	}
	game->scene = SCENE_HINTS;
	game->current_question = idx;
	game->current_hint_amt = 0;
	game->hints_start_time = SDL_GetTicks();
	game->questions[game->round][idx].is_played = 1;
	return 0;
}

int reveal_hint(const int idx) {
	if (idx < 0 || idx > 3) {
		log_err("invalid hint requested (%d)", idx);
		return 2;
	}
	if (idx != game->current_hint_amt) {
		log_err("hint %d (0-indexed) requested, but %d hints are shown", idx, game->current_hint_amt);
		return 2;
	}
	++(game->current_hint_amt);
	return 0;
}

int switch_to_steal(void) {
	game->turn_team = TEAM_BLUE + TEAM_RED - game->turn_team;
	const int hint_amt = game->round == 0? 4 : 3;
	if (game->current_hint_amt != hint_amt) {
		log_wtf("not all hints were sent over (%d/%d)", game->current_hint_amt, hint_amt);
	}
	game->scene = SCENE_HINTS_STEAL;
	return 0;
}

int switch_to_buzzed(void) {
	game->scene = SCENE_HINTS_BUZZED;
	return 0;
}

int get_hints_pts(const int hint_amt) {
	if (game->round < 0 || game->round > 1) {
		log_wtf("round mismatch (%d)", game->round);
		return -1;
	}
	if (game->scene == SCENE_HINTS_STEAL) return 1;
	if (hint_amt == 4) return 1;
	if (hint_amt == 3) return 2;
	if (hint_amt == 2) return 3;
	if (hint_amt == 1) return 5;
	log_wtf("scene = %d, hint_amt = %d", game->scene, hint_amt);
	return -1;
}

int on_hints_buzzed_correct(const int hint_amt) {
	const int pts = get_hints_pts(hint_amt);
	if (pts <= 0) {
		log_err("failed to determine points");
		return 2;
	}
	game->score[game->turn_team] += pts;
	game->turn_team = TEAM_BLUE + TEAM_RED - game->turn_team;
	game->scene = SCENE_HINTS_ANSWER;
	return 0;
}

int on_hints_steal_correct(void) {
	++(game->score[game->turn_team]);
	game->scene = SCENE_HINTS_ANSWER;
	return 0;
}

int start_next_task(void) {
	int played_amt = 0;
	switch (game->round) {
		case 0:
			/* fallthrough */
		case 1:
			for (int i = 0; i < 6; ++i) {
				if (game->questions[game->round][i].is_played) {
					++played_amt;
				}
			}
			if (played_amt == 6) ++(game->round);
			game->scene = game->round < 2? SCENE_QUESTIONS : SCENE_GAMEOVER;
			break;
		default:
			log_wtf("default case reached");
			return 1;
	}
	return 0;
}

int is_host;
int recv_thrd_stop;
struct evq *recv_queue, *send_queue;

int base_init(void) {
	if (SDL_Init(0) != 0) {
		log_err("failed to init SDL (%s)", SDL_GetError());
		goto failed_SDL_Init;
	}
	if (!(log_mtx = SDL_CreateMutex())) goto failed_mtx_init_log;
	if (!(game_state_mtx = SDL_CreateMutex())) goto failed_mtx_init_game_state;
	if (evq_init(&recv_queue)) goto failed_evq_init_recv;
	if (evq_init(&send_queue)) goto failed_evq_init_send;
	if (!(game = (struct game_state *)malloc(sizeof(struct game_state)))) {
		log_err("failed to allocate memory");
		goto failed_malloc_game_state;
	}
	if (init_game_state(game)) goto failed_init_game_state;
	recv_thrd_stop = 0;
	log_dbg("success");
	return 0;
failed_init_game_state:
	free(game);
failed_malloc_game_state:
	evq_destroy(send_queue);
failed_evq_init_send:
	evq_destroy(recv_queue);
failed_evq_init_recv:
	SDL_DestroyMutex(game_state_mtx);
failed_mtx_init_game_state:
	SDL_DestroyMutex(log_mtx);
failed_mtx_init_log:
	SDL_Quit();
failed_SDL_Init:
	return 1;
}

void base_deinit(void) {
	free(game);
	evq_destroy(send_queue);
	evq_destroy(recv_queue);
	SDL_DestroyMutex(game_state_mtx);
	SDL_DestroyMutex(log_mtx);
	log_dbg("done");
}

