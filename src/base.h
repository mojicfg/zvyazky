#pragma once
#include <SDL.h> // for timer's `Uint32` and multithreading
#include <stdio.h>
#include "event-queue.h"

int base_init(void);
void base_deinit(void);

#define LOG_MSG_LEN 126
extern char log_msg[LOG_MSG_LEN];
extern SDL_mutex *log_mtx;
#define SEP ":"
#define log_prefixed(f, pfx, fmt, ...) flogf(f, pfx SEP "%s" SEP fmt, __func__, ##__VA_ARGS__)
#define log_msg(...) flogf(stdout, __VA_ARGS__)
#define log_dbg(...) log_prefixed(stdout, "debug", __VA_ARGS__)
#define log_err(...) log_prefixed(stderr, "error", __VA_ARGS__)
#define log_wtf(...) log_prefixed(stderr, "weird", __VA_ARGS__)
void flogf(FILE *file, const char *format, ...);

enum scene_id {
	SCENE_QUESTIONS,
	SCENE_HINTS,
	SCENE_HINTS_BUZZED,
	SCENE_HINTS_STEAL,
	SCENE_HINTS_ANSWER,
	SCENE_GAMEOVER,
	SCENE_CONNLOST,
};
#define HINTS_MS 15000
#define HINT_LEN 30
#define ANSWER_LEN 62
#define TEAM_BLUE 0
#define TEAM_RED 1
#define TEAM_NAME_LEN 14
struct hint_question {
	int is_played;
	char hints[4][HINT_LEN + 1]; // hints[3] is the actual answer in the second round
	char answer[ANSWER_LEN + 1]; // and this is just an explanation
};
extern struct game_state {
	enum scene_id scene;
	int score[2];
	int round;
	int current_question;
	int current_hint_amt;
	int turn_team;
	struct hint_question questions[2][6];
	char team_name[2][TEAM_NAME_LEN + 1];
	Uint32 hints_start_time; // SCENE_HINTS timer
} *game;

void lock_game_state(void);
void unlock_game_state(void);
int push_team_name(const int team, const char);
int pop_team_name(const int team);
int select_question(const int);
int reveal_hint(const int);
int switch_to_steal(void);
int switch_to_buzzed(void);
int get_hints_pts(const int hint_amt);
int on_hints_buzzed_correct(const int hint_amt);
int on_hints_steal_correct(void);
int start_next_task(void);

// inter-thread communication
extern int is_host;
extern int recv_thrd_stop;
extern struct evq *recv_queue, *send_queue;

