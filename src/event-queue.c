#include <SDL.h> // for multithreading
#include <stdlib.h>
#include "event-queue.h"

struct evq {
	SDL_mutex *mtx;
	SDL_cond *cnd;
	struct evq_vtx *head, *tail;
};

struct evq_vtx {
	struct evq_el el;
	struct evq_vtx *next;
};

int evq_init(struct evq **q) {
	if (!q) return 2;
	if (*q) free(*q);
	*q = malloc(sizeof(struct evq));
	if (!(*q)) goto failed_malloc;
	if (!((*q)->mtx = SDL_CreateMutex())) goto failed_mtx_init;
	if (!((*q)->cnd = SDL_CreateCond())) goto failed_cnd_init;
	(*q)->head = (*q)->tail = NULL;
	return 0;
	SDL_DestroyCond((*q)->cnd);
failed_cnd_init:
	SDL_DestroyMutex((*q)->mtx);
failed_mtx_init:
	free(*q);
	*q = NULL;
failed_malloc:
	return 1;
};

void evq_free(struct evq *q) {
	if (!q) return;
	evq_lock(q);
	while (q->head) {
		struct evq_vtx *next = q->head->next;
		free(q->head);
		q->head = next;
	}
	evq_unlock(q);
}

void evq_destroy(struct evq *q) {
	if (!q) return;
	evq_free(q);
	SDL_DestroyCond(q->cnd);
	SDL_DestroyMutex(q->mtx);
	free(q);
}

void evq_lock(struct evq *q) {
	if (!q) return;
	SDL_LockMutex(q->mtx);
}

void evq_unlock(struct evq *q) {
	if (!q) return;
	SDL_UnlockMutex(q->mtx);
}

int evq_push(struct evq *q, const struct evq_el el) {
	if (!q) return 2;
	evq_lock(q);
	struct evq_vtx *vtx = (struct evq_vtx *)malloc(sizeof(struct evq_vtx));
	if (!vtx) return 1;
	vtx->el = el;
	vtx->next = NULL;
	if (q->tail) {
		q->tail = q->tail->next = vtx;
	} else {
		q->head = q->tail = vtx;
	}
	SDL_CondSignal(q->cnd);
	evq_unlock(q);
	return 0;
}

void evq_await_push(struct evq *q) {
	if (!q) return;
	SDL_CondWait(q->cnd, q->mtx);
}

struct evq_el evq_pop(struct evq *q) {
	if (!q) return EVQ_EL(EV_QUEUE_EMPTY);
	evq_lock(q);
	struct evq_el el;
	if (q->head) {
		el = q->head->el;
		struct evq_vtx *vtx = q->head;
		if (q->head->next) {
			q->head = q->head->next;
		} else {
			q->head = q->tail = NULL;
		}
		free(vtx);
	} else {
		el = EVQ_EL(EV_QUEUE_EMPTY);
	}
	evq_unlock(q);
	return el;
}

