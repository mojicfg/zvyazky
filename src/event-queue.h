#pragma once

struct evq;
struct evq_el {
	unsigned char type;
	union {
		struct {
			int idx;
		} question_selected;
		struct {
			int round;
			int question;
			int idx;
		} hint;
		struct {
			int round;
			int idx;
		} hints_answer;
		struct {
			int hint_amt;
		} hints_buzzed_correct;
		struct {
			int team;
			char input;
		} team_name_push;
		struct {
			int team;
		} team_name_pop;
	} as;
};
#define EVQ_EL(x, ...) ((struct evq_el){.type = (x), ##__VA_ARGS__})

#define EV_QUEUE_EMPTY 0xAE
#define EV_QUIT 0xE8
#define EV_BUZZ 0xB2

#define EV_QUESTION_SELECTED 0x5E
#define EV_HINT 0x41
#define EV_HINTS_TIME 0x47
#define EV_TEAM_NAME_PUSH 0x7B
#define EV_TEAM_NAME_POP 0x7C
#define EV_HINTS_ANSWER 0x4A
#define EV_HINTS_BUZZED_CORRECT 0x4C
#define EV_HINTS_BUZZED_WRONG 0x4D
#define EV_HINTS_STEAL_CORRECT 0x5C
#define EV_NEXT_TASK 0x60

int evq_init(struct evq **);
void evq_destroy(struct evq *);
void evq_lock(struct evq *);
void evq_unlock(struct evq *);
int evq_push(struct evq *, const struct evq_el);
void evq_await_push(struct evq *);
struct evq_el evq_pop(struct evq *);

