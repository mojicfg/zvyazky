#include <SDL.h>
#include <stdlib.h>
#include "base.h"
#include "net.h"
#include "ui.h"

int main(int argc, char **argv) {
	is_host = (argc == 1);
	if (base_init()) return 1;
	atexit(base_deinit);

	if (argc > 2) {
		log_msg("Usage: ./app [ip]");
		return 2;
	}

	if (net_init(is_host? NULL: argv[1])) return 1;
	atexit(net_deinit);

	if (ui_init()) return 1;
	atexit(ui_deinit);

	SDL_Thread *recv_thrd, *send_thrd;
	recv_thrd = SDL_CreateThread(is_host? server_recv_thrdf : client_recv_thrdf, "recv_thrd", NULL);
	send_thrd = SDL_CreateThread(send_thrdf, "send_thrd", NULL);

	int thrd_rc;
	thrd_rc = ui_thrdf();
	SDL_WaitThread(recv_thrd, &thrd_rc);
	SDL_WaitThread(send_thrd, &thrd_rc);

	return 0;
}

