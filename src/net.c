#include <SDL.h>
#include <SDL_net.h>
#include "base.h"
#include "net.h"

#define PORT 8978

#define CODE_QUIT EV_QUIT
#define CODE_BUZZ EV_BUZZ
#define CODE_QUESTION_SELECTED EV_QUESTION_SELECTED
#define CODE_HINT EV_HINT
#define CODE_HINTS_TIME EV_HINTS_TIME
#define CODE_HINTS_ANSWER EV_HINTS_ANSWER
#define CODE_HINTS_BUZZED_CORRECT EV_HINTS_BUZZED_CORRECT
#define CODE_HINTS_BUZZED_WRONG EV_HINTS_BUZZED_WRONG
#define CODE_HINTS_STEAL_CORRECT EV_HINTS_STEAL_CORRECT
#define CODE_NEXT_TASK EV_NEXT_TASK
#define CODE_TEAM_NAME_BLUE EV_TEAM_NAME_PUSH
#define CODE_TEAM_NAME_RED EV_TEAM_NAME_POP

TCPsocket sock;

#define NESTED_SOURCE
#include "net/deserialize.c"
#undef NESTED_SOURCE

int client_recv_thrdf(void *arg) {
	(void)arg;
	log_dbg("started");
	unsigned char code = 0;
	while (1) {
		if (SDLNet_TCP_Recv(sock, &code, 1) != 1) {
			log_err("could not recv from server");
			lock_game_state();
			game->scene = SCENE_CONNLOST;
			unlock_game_state();
			goto thrdf_stop;
		}
		int team = TEAM_RED; // for CODE_TEAM_NAME packets
		lock_game_state();
		switch (code) {
			case CODE_QUIT:
				// the client side can just ignore this
				break;
			case CODE_BUZZ:
				switch_to_buzzed();
				break;
			case CODE_TEAM_NAME_BLUE:
				team = TEAM_BLUE;
				/* fallthrough */
			case CODE_TEAM_NAME_RED:
				recv_team_name(sock, team);
				break;
			case CODE_QUESTION_SELECTED:
				recv_question_selected(sock);
				break;
			case CODE_HINT:
				recv_hint(sock);
				break;
			case CODE_HINTS_TIME:
				switch_to_steal();
				break;
			case CODE_HINTS_ANSWER:
				recv_hints_answer(sock);
				if (game->scene == SCENE_HINTS_STEAL) {
					// the steal was wrong
					game->scene = SCENE_HINTS_ANSWER;
				}
				break;
			case CODE_HINTS_BUZZED_CORRECT:
				recv_hints_buzzed_correct(sock);
				break;
			case CODE_HINTS_BUZZED_WRONG:
				switch_to_steal();
				break;
			case CODE_HINTS_STEAL_CORRECT:
				on_hints_steal_correct();
				break;
			case CODE_NEXT_TASK:
				start_next_task();
				break;
			default:
				log_wtf("unexpected code received (0x%02X)", code);
		}
		unlock_game_state();
	}
thrdf_stop:
	log_dbg("stopped");
	return 0;
}

SDLNet_SocketSet set;
#define MAXCLIENTS 6
struct client {
	TCPsocket sock;
} clients[MAXCLIENTS];
int client_amt;
SDL_mutex *clients_mtx;

void welcome_client(TCPsocket client_sock) {
	SDL_LockMutex(clients_mtx);
	if (client_amt == MAXCLIENTS) {
		log_dbg("connection refused (client limit reached)");
		SDLNet_TCP_Close(client_sock);
		return;
	}
	if (SDLNet_TCP_AddSocket(set, client_sock) != client_amt + 1) {
		log_err("could not add a socket to the set");
		log_dbg("connection refused (could not add a socket to the set)");
		SDLNet_TCP_Close(client_sock);
		return;
	}
	clients[client_amt++] = (struct client){.sock = client_sock};
	// TODO assign captains
	log_dbg("success");
	// TODO send game info
	SDL_UnlockMutex(clients_mtx);
}

void drop_client(const int idx) {
	SDL_LockMutex(clients_mtx);
	if (idx < 0 || idx >= client_amt) {
		log_wtf("idx = %d", idx);
		return;
	}
	if (SDLNet_TCP_DelSocket(set, clients[idx].sock) != client_amt - 1) {
		log_err("could not delete a socket from the set");
		// XXX this isn't handled yet
	}
	SDLNet_TCP_Close(clients[idx].sock);
	// TODO update captains
	clients[idx] = clients[--client_amt];
	clients[client_amt] = (struct client){.sock = NULL};
	log_dbg("success");
	SDL_UnlockMutex(clients_mtx);
}

#define POLL_MS 100
int server_recv_thrdf(void *arg) {
	(void)arg;
	log_dbg("started");
	TCPsocket new_client;
	unsigned char code;
	while (!recv_thrd_stop) {
		if ((new_client = SDLNet_TCP_Accept(sock))) welcome_client(new_client);
		int amt = SDLNet_CheckSockets(set, POLL_MS);
		if (!client_amt || !amt) continue;
		if (amt == -1) {
			// `client_amt` != 0 => error
			log_err("error when checking sockets for activity");
			goto thrdf_stop;
		}
		SDL_LockMutex(clients_mtx);
		for (int i = 0; i < client_amt; ++i) {
			if (SDLNet_SocketReady(clients[i].sock)) {
				--amt;
				if (SDLNet_TCP_Recv(clients[i].sock, &code, 1) != 1) {
					log_err("could not recv from client");
					drop_client(i);
					--i; // there's now a new client to process at this index
					continue;
				}
				switch (code) {
					case CODE_QUIT:
						log_dbg("client quit");
						drop_client(i);
						--i;
						break;
					case CODE_BUZZ:
						lock_game_state();
						// TODO check for the right to buzz
						switch_to_buzzed();
						evq_push(send_queue, EVQ_EL(EV_BUZZ)); // broadcast it back to everyone
						unlock_game_state();
						break;
					default:
						log_wtf("unexpected code received (0x%02X)", code);
				}
			}
		}
		if (amt) log_wtf("amt mismatch (%d)", amt);
		SDL_UnlockMutex(clients_mtx);
	}
thrdf_stop:
	log_dbg("disconnecting all clients");
	while (client_amt) drop_client(0);
	log_dbg("stopped");
	return 0;
}

#define NESTED_SOURCE
#include "net/serialize.c"
#undef NESTED_SOURCE

int send_thrdf(void *arg) {
	(void)arg;
	evq_lock(send_queue);
	log_dbg("started");
	struct evq_el ev;
	while (1) {
		evq_await_push(send_queue);
		while (ev = evq_pop(send_queue), ev.type != EV_QUEUE_EMPTY) {
			if (ev.type == EV_QUIT) {
				SEND(send_code_only, CODE_QUIT);
				goto thrdf_stop;
			}
			lock_game_state();
			switch (ev.type) {
				case EV_BUZZ:
					SEND(send_code_only, CODE_BUZZ);
					break;
				case EV_TEAM_NAME_PUSH:
					if (!push_team_name(ev.as.team_name_push.team, ev.as.team_name_push.input)) {
						SEND(send_team_name, ev.as.team_name_push.team);
					}
					break;
				case EV_TEAM_NAME_POP:
					if (!pop_team_name(ev.as.team_name_pop.team)) {
						SEND(send_team_name, ev.as.team_name_pop.team);
					}
					break;
				case EV_QUESTION_SELECTED:
					if (!select_question(ev.as.question_selected.idx)) {
						SEND(send_question_selected, ev.as.question_selected.idx);
					}
					break;
				case EV_HINT:
					if (!reveal_hint(ev.as.hint.idx)) {
						SEND(send_hint, ev.as.hint.round, ev.as.hint.question, ev.as.hint.idx);
					}
					break;
				case EV_HINTS_TIME:
					if (!switch_to_steal()) {
						SEND(send_code_only, CODE_HINTS_TIME);
					}
					break;
				case EV_HINTS_ANSWER:
					if (game->scene == SCENE_HINTS_STEAL) {
						// the steal was wrong
						game->scene = SCENE_HINTS_ANSWER;
					}
					SEND(send_hints_answer, ev.as.hints_answer.round, ev.as.hints_answer.idx);
					break;
				case EV_HINTS_BUZZED_CORRECT:
					if (!on_hints_buzzed_correct(ev.as.hints_buzzed_correct.hint_amt)) {
						SEND(send_hints_buzzed_correct, ev.as.hints_buzzed_correct.hint_amt);
					}
					break;
				case EV_HINTS_BUZZED_WRONG:
					if (!switch_to_steal()) {
						SEND(send_code_only, CODE_HINTS_BUZZED_WRONG);
					}
					break;
				case EV_HINTS_STEAL_CORRECT:
					if (!on_hints_steal_correct()) {
						SEND(send_code_only, CODE_HINTS_STEAL_CORRECT);
					}
					break;
				case EV_NEXT_TASK:
					if (!start_next_task()) {
						SEND(send_code_only, CODE_NEXT_TASK);
					}
					break;
				default:
					log_wtf("unexpected event type (0x%02X)", ev.type);
			}
			unlock_game_state();
		}
	}
thrdf_stop:
	evq_unlock(send_queue);
	log_dbg("stopped");
	return 0;
}

int net_init(const char *addr) { // `addr` is `NULL` when starting a server
	if (SDL_Init(0) != 0) {
		log_err("failed to init SDL (%s)", SDL_GetError());
		goto failed_SDL_Init;
	}
	if (SDLNet_Init() != 0) {
		log_err("failed to init SDL_net (%s)", SDLNet_GetError());
		goto failed_SDLNet_Init;
	}
	IPaddress sockaddr;
	if (SDLNet_ResolveHost(&sockaddr, addr, PORT) != 0) {
		log_err("could not resolve host (%s)", SDLNet_GetError());
		goto failed_SDLNet_ResolveHost;
	}
	if (!(sock = SDLNet_TCP_Open(&sockaddr))) {
		log_err("failed to open TCP socket (%s)", SDLNet_GetError());
		goto failed_SDLNet_ResolveHost;
	}
	if (!addr) {
		// initialize server side variables
		if (!(set = SDLNet_AllocSocketSet(MAXCLIENTS))) {
			log_err("failed to allocate SDLNet_SocketSet (%s)", SDLNet_GetError());
			goto failed_SDLNet_AllocSocketSet;
		}
		if (!(clients_mtx = SDL_CreateMutex())) {
			log_err("failed to initialize clients_mtx");
			goto failed_mtx_init_clients;
		}
		for (int i = 0; i < MAXCLIENTS; ++i) {
			clients[i].sock = NULL;
		}
		client_amt = 0;
		// TODO init captains
	}
	log_dbg("success");
	return 0;
	SDL_DestroyMutex(clients_mtx);
failed_mtx_init_clients:
	SDLNet_FreeSocketSet(set);
failed_SDLNet_AllocSocketSet:
	SDLNet_TCP_Close(sock);
failed_SDLNet_ResolveHost:
	SDLNet_Quit();
failed_SDLNet_Init:
	SDL_Quit();
failed_SDL_Init:
	return 1;
}

void net_deinit(void) {
	if (is_host) {
		SDL_DestroyMutex(clients_mtx);
		SDLNet_FreeSocketSet(set);
	}
	SDLNet_TCP_Close(sock);
	SDLNet_Quit();
	log_dbg("done");
}

