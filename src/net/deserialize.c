#ifndef NESTED_SOURCE
#error this file is not supposed to be compiled on its own
#endif
#define RECV_PACKET(len) \
	const int packet_len = len;\
	char packet[packet_len];\
	if (SDLNet_TCP_Recv(sock, packet, packet_len) < packet_len) {\
		log_err("failed to recv");\
		return 1;\
	}

int recv_team_name(TCPsocket sock, const int team) {
	RECV_PACKET(TEAM_NAME_LEN + 1);
	strncpy(game->team_name[team], packet, TEAM_NAME_LEN + 1);
	return 0;
}

int recv_question_selected(TCPsocket sock) {
	RECV_PACKET(1);
	const int idx = (int)packet[0];
	select_question(idx);
	return 0;
}

int recv_hint(TCPsocket sock) {
	RECV_PACKET(1 + 1 + 1 + HINT_LEN + 1);
	const int round = (int)packet[0];
	const int question = (int)packet[1];
	const int idx = (int)packet[2];
	if (round < 0 || round > 1) {
		log_wtf("round mismatch (%d)", round);
		return 2;
	}
	strncpy(game->questions[round][question].hints[idx], packet + 3, HINT_LEN + 1);
	reveal_hint(idx);
	return 0;
}

int recv_hints_answer(TCPsocket sock) {
	RECV_PACKET(1 + 1 + ANSWER_LEN + 1);
	const int round = (int)packet[0];
	const int idx = (int)packet[1];
	if (round < 0 || round > 1) {
		log_wtf("round mismatch (%d)", round);
		return 2;
	}
	strncpy(game->questions[round][idx].answer, packet + 2, ANSWER_LEN + 1);
	return 0;
}

int recv_hints_buzzed_correct(TCPsocket sock) {
	RECV_PACKET(1);
	const int hint_amt = (int)packet[0];
	on_hints_buzzed_correct(hint_amt);
	return 0;
}

