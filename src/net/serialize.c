#ifndef NESTED_SOURCE
#error this file is not supposed to be compiled on its own
#endif

#define SEND(sendf, ...)\
	if (is_host) {\
		for (int i = 0; i < client_amt; ++i) {\
			if (sendf(clients[i].sock, ##__VA_ARGS__)) {\
				drop_client(i);\
				--i;\
			}\
		}\
	} else {\
		sendf(sock, ##__VA_ARGS__);\
	}
// XXX possible data races, should `clients_mtx` be recursive and lock `sendf` and `drop_client` together?

int send_code_only(TCPsocket sock, const unsigned char code) {
	if (SDLNet_TCP_Send(sock, &code, 1) < 1) {
		log_err("failed to send");
		return 1;
	}
	return 0;
}

int send_packet(TCPsocket sock, const char *packet, const int packet_len) {
	if (SDLNet_TCP_Send(sock, packet, packet_len) < packet_len) {
		log_err("failed to send");
		return 1;
	}
	return 0;
}

int send_team_name(TCPsocket sock, const int team) {
	const int packet_len = 1 + TEAM_NAME_LEN + 1;
	char packet[packet_len];
	packet[0] = (char)(team == TEAM_BLUE? CODE_TEAM_NAME_BLUE : CODE_TEAM_NAME_RED);
	strncpy(packet + 1, game->team_name[team], TEAM_NAME_LEN + 1);
	return send_packet(sock, packet, packet_len);
}

int send_question_selected(TCPsocket sock, const int idx) {
	const int packet_len = 1 + 1;
	char packet[packet_len];
	packet[0] = (char)CODE_QUESTION_SELECTED;
	packet[1] = (char)idx;
	return send_packet(sock, packet, packet_len);
}

int send_hint(TCPsocket sock, const int round, const int question, const int idx) {
	if (round < 0 || round > 1) {
		log_wtf("round mismatch (%d)", round);
		return 2;
	}
	const int packet_len = 1 + 1 + 1 + 1 + HINT_LEN + 1;
	char packet[packet_len];
	packet[0] = (char)CODE_HINT;
	packet[1] = (char)round;
	packet[2] = (char)question;
	packet[3] = (char)idx;
	strncpy(packet + 4, game->questions[round][question].hints[idx], HINT_LEN + 1);
	return send_packet(sock, packet, packet_len);
}

int send_hints_answer(TCPsocket sock, const int round, const int idx) {
	if (round < 0 || round > 1) {
		log_wtf("round mismatch (%d)", round);
		return 2;
	}
	const int packet_len = 1 + 1 + 1 + ANSWER_LEN + 1;
	char packet[packet_len];
	packet[0] = (char)CODE_HINTS_ANSWER;
	packet[1] = (char)round;
	packet[2] = (char)idx;
	strncpy(packet + 3, game->questions[round][idx].answer, ANSWER_LEN + 1);
	return send_packet(sock, packet, packet_len);
}

int send_hints_buzzed_correct(TCPsocket sock, const int hint_amt) {
	const int packet_len = 1 + 1;
	char packet[packet_len];
	packet[0] = (char)CODE_HINTS_BUZZED_CORRECT;
	packet[1] = (char)hint_amt;
	return send_packet(sock, packet, packet_len);
}

