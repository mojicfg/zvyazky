#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "base.h"
#include "ui.h"

#define WINDOW_TITLE "Zvyazky"
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

const int FPS_MAX = 60;
const Uint32 MS_PER_FRAME = 1000 / FPS_MAX;

SDL_Window *win;
SDL_Renderer *rend;
struct game_state ui_game_view; // a local copy for the duration of a frame

enum kbrd_dest {
	KBRD_TO_GAME,
	KBRD_TO_TEAM_NAME_BLUE,
	KBRD_TO_TEAM_NAME_RED,
} current_kbrd_dest; // where keyboard input currently goes

// yes, this is what I do with my life
#define NESTED_SOURCE
#include "ui/texture.c"
#include "ui/render.c"
#undef NESTED_SOURCE

int ui_init(void) {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		log_err("failed to init SDL (%s)", SDL_GetError());
		goto failed_SDL_Init;
	}
	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		log_err("failed to init SDL_image");
		goto failed_IMG_Init;
	}
	if (TTF_Init() != 0) {
		log_err("failed to init SDL_ttf");
		goto failed_TTF_Init;
	}
	win = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	if (!win) {
		log_err("failed to create window (%s)", SDL_GetError());
		goto failed_SDL_CreateWindow;
	}
	if (!(rend = SDL_CreateRenderer(win, -1, 0))) {
		log_err("failed to create renderer (%s)", SDL_GetError());
		goto failed_SDL_CreateRenderer;
	}
	if (load_fonts()) {
		log_err("failed to load fonts");
		goto failed_load_fonts;
	}
	if (load_tx_assets()) {
		log_err("failed to load textures");
		goto failed_load_tx_assets;
	}
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 3; ++j) {
			const int idx = i * 3 + j;
			// this is how `tx_questions_rect` is used to
			// calculate both source and destination rects
			tx_questions_screen_rects[idx] = tx_questions_rect;
			tx_questions_screen_rects[idx].y += tx_questions_rect.h * i;
			tx_questions_screen_rects[idx].x += tx_questions_rect.w * j;
			tx_questions_spritesheet_rects[idx] = tx_questions_screen_rects[idx];
			tx_questions_spritesheet_rects[idx].y -= tx_questions_rect.y;
			tx_questions_spritesheet_rects[idx].x -= tx_questions_rect.x;
		}
	}
	for (int i = 0; i < 4; ++i) {
		tx_hints_rects[i] = tx_hints_rect;
		tx_hints_rects[i].x += tx_hints_rect.w * i;
	}
	current_kbrd_dest = KBRD_TO_GAME;
	log_dbg("success");
	return 0;
failed_load_tx_assets:
	free_tx_assets(); // put after the label intentionally
	close_fonts();
failed_load_fonts:
	SDL_DestroyRenderer(rend);
failed_SDL_CreateRenderer:
	SDL_DestroyWindow(win);
failed_SDL_CreateWindow:
	TTF_Quit();
failed_TTF_Init:
	IMG_Quit();
failed_IMG_Init:
	SDL_Quit();
failed_SDL_Init:
	return 1;
}

void ui_deinit(void) {
	free_tx_assets();
	close_fonts();
	if (rend) SDL_DestroyRenderer(rend);
	else log_wtf("no renderer to destroy");
	if (win) SDL_DestroyWindow(win);
	else log_wtf("no window to destroy");
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	log_dbg("done");
}

void reveal_all_hints(void) {
	const int hint_amt = ui_game_view.round == 0? 4 : 3;
	for (int i = ui_game_view.current_hint_amt; i < hint_amt; ++i) {
		evq_push(send_queue, EVQ_EL(EV_HINT,
					.as.hint = {
					.round = ui_game_view.round,
					.question = ui_game_view.current_question,
					.idx = i}));
	}
}

int ui_thrdf(void) {
	log_dbg("started");
	Uint32 start_time, end_time; // used to cap fps
	Uint32 hints_elapsed_time; // SCENE_HINTS timer
	SDL_Event ev;
	int selected_question = NO_QUESTION_SELECTED;
	while (1) {
		start_time = SDL_GetTicks();
		lock_game_state(); // XXX locking the mutex every frame
		ui_game_view = *game;
		unlock_game_state();
		if (ui_game_view.scene == SCENE_HINTS) {
			hints_elapsed_time = start_time - ui_game_view.hints_start_time;
			if (is_host && hints_elapsed_time >= HINTS_MS) {
				reveal_all_hints();
				evq_push(send_queue, EVQ_EL(EV_HINTS_TIME));
			}
		}
		is_status_displayed = 0;
		while (SDL_PollEvent(&ev)) {
			switch (ev.type) {
				case SDL_QUIT:
					evq_push(send_queue, EVQ_EL(EV_QUIT)); // make send_thrdf stop
					recv_thrd_stop = 1; // make server_recv_thrdf stop
					goto thrdf_stop;
					break;
				case SDL_KEYDOWN:
					if (current_kbrd_dest == KBRD_TO_GAME) {
						switch (ui_game_view.scene) {
							case SCENE_HINTS:
								if (is_host) {
									if (ev.key.keysym.sym == SDLK_SPACE) {
										// host reveals the next hint
										const int hint_amt = ui_game_view.round == 0? 4 : 3;
										if (ui_game_view.current_hint_amt < hint_amt) {
											evq_push(send_queue, EVQ_EL(EV_HINT,
														.as.hint = {
														.round = ui_game_view.round,
														.question = ui_game_view.current_question,
														.idx = ui_game_view.current_hint_amt}));
										}
									}
								} else {
									if (ev.key.keysym.sym == SDLK_SPACE) {
										// player buzzes
										evq_push(send_queue, EVQ_EL(EV_BUZZ));
									}
								}
								break;
							case SCENE_HINTS_BUZZED:
								if (is_host) {
									if (ev.key.keysym.sym == SDLK_y) {
										reveal_all_hints();
										if (ui_game_view.round == 1) {
											// in the second round, send out the last hint (the answer)
											evq_push(send_queue, EVQ_EL(EV_HINT,
														.as.hint = {
														.round = ui_game_view.round,
														.question = ui_game_view.current_question,
														.idx = 3}));
										}
										evq_push(send_queue, EVQ_EL(EV_HINTS_ANSWER,
													.as.hints_answer = {
													.round = ui_game_view.round,
													.idx = ui_game_view.current_question}));
										evq_push(send_queue, EVQ_EL(EV_HINTS_BUZZED_CORRECT,
													.as.hints_buzzed_correct.hint_amt = ui_game_view.current_hint_amt));
									} else if (ev.key.keysym.sym == SDLK_n) {
										reveal_all_hints();
										evq_push(send_queue, EVQ_EL(EV_HINTS_BUZZED_WRONG));
									}
								}
								break;
							case SCENE_HINTS_STEAL:
								if (is_host) {
									switch (ev.key.keysym.sym) {
										case SDLK_y:
											evq_push(send_queue, EVQ_EL(EV_HINTS_STEAL_CORRECT));
											/* fallthrough */
										case SDLK_n:
											if (ui_game_view.round == 1) {
												// in the second round, send out the last hint (the answer)
												evq_push(send_queue, EVQ_EL(EV_HINT,
															.as.hint = {
															.round = ui_game_view.round,
															.question = ui_game_view.current_question,
															.idx = 3}));
											}
											evq_push(send_queue, EVQ_EL(EV_HINTS_ANSWER,
														.as.hints_answer = {
														.round = ui_game_view.round,
														.idx = ui_game_view.current_question}));
											break;
									}
								}
								break;
							case SCENE_HINTS_ANSWER:
								if (is_host) {
									if (ev.key.keysym.sym == SDLK_SPACE) {
										evq_push(send_queue, EVQ_EL(EV_NEXT_TASK));
									}
								}
								break;
							default:
								break;
						}
					} else {
						// key pressed in team name editing mode
						int team = (current_kbrd_dest == KBRD_TO_TEAM_NAME_BLUE? TEAM_BLUE : TEAM_RED);
						char kbrd_input = 0;
						const SDL_Keycode input_keycodes[] = {
							SDLK_a, SDLK_b, SDLK_c, SDLK_d, SDLK_e, SDLK_f, SDLK_g, SDLK_h,	SDLK_i,
							SDLK_j, SDLK_k, SDLK_l, SDLK_m, SDLK_n, SDLK_o, SDLK_p, SDLK_q, SDLK_r,
							SDLK_s, SDLK_t, SDLK_u, SDLK_v, SDLK_w, SDLK_x, SDLK_y, SDLK_z,
							SDLK_SPACE
						};
						const char input_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
						const size_t len = sizeof(input_keycodes) / sizeof(SDL_Keycode);
						switch (ev.key.keysym.sym) {
							case SDLK_ESCAPE:
								// pressing escape quits team name editing
								current_kbrd_dest = KBRD_TO_GAME;
								break;
							case SDLK_BACKSPACE:
								// backspace deletes the last character
								evq_push(send_queue, EVQ_EL(EV_TEAM_NAME_POP,
											.as.team_name_pop.team = team));
								break;
							default:
								kbrd_input = 0;
								for (size_t i = 0; i < len; ++i) {
									if (ev.key.keysym.sym == input_keycodes[i]) {
										kbrd_input = input_chars[i];
										break;
									}
								}
								if (kbrd_input) {
									evq_push(send_queue, EVQ_EL(EV_TEAM_NAME_PUSH,
												.as.team_name_push = {
												.team = team, .input = kbrd_input}));
								}
						}
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					if (is_host) {
						SDL_Point mouse_pos = (SDL_Point){ev.button.x, ev.button.y};
						// host may click team name boards to get to the team name editing mode
						if (SDL_PointInRect(&mouse_pos, tx_rects + TX_TEAM_BLUE) == SDL_TRUE) {
							current_kbrd_dest = KBRD_TO_TEAM_NAME_BLUE;
						} else if (SDL_PointInRect(&mouse_pos, tx_rects + TX_TEAM_RED) == SDL_TRUE) {
							current_kbrd_dest = KBRD_TO_TEAM_NAME_RED;
						}
					}
					break;
				case SDL_MOUSEBUTTONUP:
					if (is_host) {
						//SDL_Point mouse_pos = (SDL_Point){ev.button.x, ev.button.y};
						if (ui_game_view.scene == SCENE_QUESTIONS && selected_question != NO_QUESTION_SELECTED) {
							evq_push(send_queue, EVQ_EL(EV_QUESTION_SELECTED,
										.as.question_selected.idx = selected_question));
							evq_push(send_queue, EVQ_EL(EV_HINT,
										.as.hint = {
										.round = ui_game_view.round,
										.question = selected_question,
										.idx = 0}));
						}
					}
					break;
			}
		}
		int mouse_x, mouse_y;
		const Uint32 mouse_btn = SDL_GetMouseState(&mouse_x, &mouse_y);
		SDL_SetRenderDrawColor(rend, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(rend);
		render_hud();
		if (current_kbrd_dest == KBRD_TO_TEAM_NAME_BLUE ||
				current_kbrd_dest == KBRD_TO_TEAM_NAME_RED) {
			STATUS("Press Esc to quit editing");
		}
		switch (ui_game_view.scene) {
			case SCENE_QUESTIONS:
				selected_question = render_question_selection(mouse_x, mouse_y, mouse_btn);
				break;
			case SCENE_HINTS:
				//STATUS("time elapsed: %d ms", hints_elapsed_time);
				render_hints(hints_elapsed_time);
				break;
			case SCENE_HINTS_BUZZED:
				if (is_host) STATUS("Is the answer correct? (y/n)");
				/* fallthrough */
			case SCENE_HINTS_STEAL:
				if (is_host) STATUS("Is the steal correct? (y/n)");
				/* fallthrough */
			case SCENE_HINTS_ANSWER:
				if (is_host) STATUS("Press Space to continue");
				render_hints(hints_elapsed_time);
				break;
			case SCENE_CONNLOST:
				STATUS("Connection has been lost");
				break;
			case SCENE_GAMEOVER:
				STATUS("Game over! Thanks for playing!");
				break;
			default:
				log_wtf("default case reached");
		}
		SDL_RenderPresent(rend);
		end_time = SDL_GetTicks();
		if (end_time < start_time) {
			// this is possible, e.g. when SDL's timer wraps around
			// whenever this happens, it should be harmless
			// let's have this case in here just to be safe
			log_wtf("end_time < start_time");
		} else if (end_time - start_time < MS_PER_FRAME) {
			// add delay to cap fps and not torment CPU/GPU
			SDL_Delay(MS_PER_FRAME - (end_time - start_time));
		}
	}
thrdf_stop:
	log_dbg("stopped");
	return 0;
}

