#ifndef NESTED_SOURCE
#error this file is not supposed to be compiled on its own
#endif

const char question_names[6][64] = {
	"Two Reeds",
	"Lion",
	"Twisted Flax",
	"Horned Viper",
	"Water",
	"Eye of Chorus",
};
const SDL_Color text_col_light = {0xFF, 0xFF, 0xFF, 0xFF};
const SDL_Color text_col_dark = {0, 0, 0, 0xFF};
int is_status_displayed;

#define CENTERED_X 1
#define CENTERED_Y 2
#define CENTERED (CENTERED_X | CENTERED_Y)
int render_text(
		SDL_Rect rect,                            // mostly, underlying texture's rect
		int flags,                                // bitwise-OR of `CENTERED*` flags above
		const enum text_id text_id,
		enum font_id font_id, const SDL_Color fg, // font parameters
		const char *format, ...                   // passed to `snprintf`
		) {
	va_list va;
	const int MAXLEN = 126;
	char text[MAXLEN + 1]; // buffer to hold constructed text
	int w, h; // texture size returned by `render_text_tx`
	va_start(va, format);
	// XXX `vsnprintf` errors not handled
	vsnprintf(text, MAXLEN + 1, format, va);
	va_end(va);
	if (strlen(text) == 0) {
		// empty strings need not apply
		return 0;
	}
	if (render_text_tx(text_id, text, font_id, fg, &w, &h)) {
		log_err("failed");
		return 1;
	}
	if (flags & CENTERED_X) {
		rect.x += (rect.w - w) / 2;
	}
	if (flags & CENTERED_Y) {
		rect.y += (rect.h - h) / 2;
	}
	rect.w = w, rect.h = h;
	SDL_RenderCopy(rend, text_txs[text_id], NULL, &rect);
	return 0;
}
#define STATUS(...) if (!is_status_displayed) {\
	render_text((const SDL_Rect){.x = 0, .y = 235, .w = WINDOW_WIDTH, .h = 0},\
			CENTERED_X, TEXT_STATUS, FONT_STATUS, text_col_dark, __VA_ARGS__);\
	is_status_displayed = 1;}

void render_hud(void) {
	for (enum tx_id i = 0; i < TX_TOTAL_HUD; ++i) {
		SDL_RenderCopy(rend, txs[i], NULL, tx_rects + i);
	}
	render_text(tx_rects[TX_SCORE_BLUE], CENTERED, TEXT_SCORE_BLUE,
			FONT_SCORE, text_col_light, "%d", ui_game_view.score[TEAM_BLUE]);
	render_text(tx_rects[TX_SCORE_RED], CENTERED, TEXT_SCORE_RED,
			FONT_SCORE, text_col_light, "%d", ui_game_view.score[TEAM_RED]);
	render_text(tx_rects[TX_TEAM_BLUE], CENTERED, TEXT_TEAM_BLUE,
			FONT_TEAM, text_col_light, "%s", ui_game_view.team_name[TEAM_BLUE]);
	render_text(tx_rects[TX_TEAM_RED], CENTERED, TEXT_TEAM_RED,
			FONT_TEAM, text_col_light, "%s", ui_game_view.team_name[TEAM_RED]);
}

#define NO_QUESTION_SELECTED -1
int render_question_selection(const int mouse_x, const int mouse_y, const Uint32 mouse_btn) {
	int selected_question = NO_QUESTION_SELECTED;
	SDL_Point mouse_pos = {.x = mouse_x, .y = mouse_y};
	for (int i = 0; i < 2; ++i)
	for (int j = 0; j < 3; ++j) {
		const int idx = i * 3 + j;
		enum tx_id tx = TX_QUESTIONS;
		if (ui_game_view.questions[ui_game_view.round][idx].is_played) {
			// if the question has already been played, make the button appear inactive
			tx = TX_QUESTIONS_OFF;
		} else if (SDL_PointInRect(&mouse_pos, tx_questions_screen_rects + idx) == SDL_TRUE) {
			// if the mouse is over the question button
			if (is_host && selected_question == NO_QUESTION_SELECTED) selected_question = idx;
			if (is_host && (mouse_btn & SDL_BUTTON(SDL_BUTTON_LEFT))) {
				// make the button highlighted on click for host
				tx = TX_QUESTIONS_INV;
				STATUS("Selected %s", question_names[idx]);
			} else {
				// make the button glow on hover (as well as on click for players)
				tx = TX_QUESTIONS_GLOW;
				STATUS("%s", question_names[idx]);
			}
		}
		SDL_RenderCopy(rend, txs[tx], tx_questions_spritesheet_rects + idx,
				tx_questions_screen_rects + idx);
	}
	if (is_host) STATUS("Select a question");
	return selected_question;
}

void render_hints(Uint32 hints_elapsed_time) {
	for (int i = 0; i < ui_game_view.current_hint_amt; ++i) {
		SDL_RenderCopy(rend, txs[TX_HINT], NULL, tx_hints_rects + i);
		render_text(tx_hints_rects[i], CENTERED, TEXT_HINT1 + i, FONT_HINT, text_col_dark,
				"%s", ui_game_view.questions[ui_game_view.round][ui_game_view.current_question].hints[i]);
	}
	if (ui_game_view.scene != SCENE_HINTS_ANSWER) {
		const int hint_idx = ui_game_view.scene == SCENE_HINTS_STEAL? 3 :
			ui_game_view.current_hint_amt - 1;
		// render the empty time bar
		SDL_Rect timer_rect = tx_hints_rects[hint_idx];
		timer_rect.w = 145, timer_rect.h = 30;
		timer_rect.y -= timer_rect.h + 4;
		timer_rect.x += 4;
		SDL_RenderCopy(rend, txs[TX_HINT_TIMER], NULL, &timer_rect);
		const SDL_Rect pts_text_rect = timer_rect;
		// render the partially filled time bar
		timer_rect.w *= hints_elapsed_time / (double) HINTS_MS;
		SDL_Rect cutout_rect = timer_rect;
		cutout_rect.x = cutout_rect.y = 0;
		SDL_RenderCopy(rend, txs[TX_HINT_TIMER_ACTIVE], &cutout_rect, &timer_rect);
		// render the points text over the time bar
		render_text(pts_text_rect, CENTERED, TEXT_HINT_TIMER_PTS,
				FONT_HINT_TIMER_PTS, text_col_light,
				(get_hints_pts(ui_game_view.current_hint_amt) > 1? "%d points" : "%d point"),
				get_hints_pts(ui_game_view.current_hint_amt));
	}
	if (is_host || ui_game_view.scene == SCENE_HINTS_ANSWER) {
		SDL_RenderCopy(rend, txs[TX_HINTS_ANSWER], NULL, tx_rects + TX_HINTS_ANSWER);
		render_text(tx_rects[TX_HINTS_ANSWER], CENTERED, TEXT_HINTS_ANSWER,
				FONT_HINTS_ANSWER, text_col_light, "%s",
				ui_game_view.questions[ui_game_view.round][ui_game_view.current_question].answer);
	}
}

