#ifndef NESTED_SOURCE
#error this file is not supposed to be compiled on its own
#endif

enum tx_id {
	// common UI elements
	TX_BG,
	TX_TEAM_BLUE,
	TX_TEAM_RED,
	TX_SCORE_BLUE,
	TX_SCORE_RED,

	TX_TOTAL_HUD, // common HUD elements end here

	TX_HINTS_ANSWER = TX_TOTAL_HUD,

	TX_TOTAL_CONST_RECTS, // textures above have their constant `tx_rects`

	// a 2 by 3 spritesheet of question buttons
	TX_QUESTIONS = TX_TOTAL_CONST_RECTS,
	TX_QUESTIONS_GLOW,
	TX_QUESTIONS_OFF,
	TX_QUESTIONS_INV,

	TX_HINT,
	TX_HINT_TIMER,
	TX_HINT_TIMER_ACTIVE,

	TX_TOTAL
};
SDL_Texture *txs[TX_TOTAL];
const char tx_filenames[TX_TOTAL][64] = {
	"assets/bg.png",
	"assets/team-board-blue.png",
	"assets/team-board-red.png",
	"assets/score-board-blue.png",
	"assets/score-board-red.png",
	"assets/answer-board.png",
	"assets/glyph.png",
	"assets/glyph-glow.png",
	"assets/glyph-off.png",
	"assets/glyph-inv.png",
	"assets/hint-board.png",
	"assets/hint-time-board.png",
	"assets/hint-time-board-active.png",
};
const SDL_Rect tx_rects[TX_TOTAL_CONST_RECTS] = {
	{.x = 0, .y = 0, .w = WINDOW_WIDTH, .h = WINDOW_HEIGHT},
	{.x = 15, .y = 15, .w = 220, .h = 39},
	{.x = WINDOW_WIDTH - 15 - 220, .y = 15, .w = 220, .h = 39},
	{.x = 15 + 220 + 10, .y = 13, .w = 67, .h = 67},
	{.x = WINDOW_WIDTH - 15 - 220 - 10 - 67, .y = 13, .w = 67, .h = 67},
	{.x = WINDOW_WIDTH / 2 - 2 * 153 + 4, .y = 300 + 101 + 5, .w = 604, .h = 30},
};
const SDL_Rect tx_questions_rect = {.x = 53, .y = 260, .w = 178, .h = 101};
SDL_Rect tx_questions_screen_rects[6], tx_questions_spritesheet_rects[6]; // calculated on init using `tx_questions_rect`
const SDL_Rect tx_hints_rect = {.x = WINDOW_WIDTH / 2 - 2 * 153, .y = 300, .w = 153, .h = 101};
SDL_Rect tx_hints_rects[4]; // calculated on init using `tx_hints_rect`

int load_tx_asset(const enum tx_id id) {
	if (txs[id]) {
		SDL_DestroyTexture(txs[id]);
	}
	SDL_Surface *surf;
	if (!(surf = IMG_Load(tx_filenames[id]))) {
		log_err("failed to load surface from file (%s)", tx_filenames[id]);
		return 1;
	}
	txs[id] = SDL_CreateTextureFromSurface(rend, surf);
	SDL_FreeSurface(surf);
	if (!txs[id]) {
		log_err("failed to create texture from surface (%s)", tx_filenames[id]);
		return 1;
	}
	return 0;
}
int load_tx_assets(void) {
	for (enum tx_id i = 0; i < TX_TOTAL; ++i) {
		if (load_tx_asset(i)) {
			return 1;
		}
	}
	return 0;
}
void free_tx_assets(void) {
	for (enum tx_id i = 0; i < TX_TOTAL; ++i) {
		if (txs[i]) {
			SDL_DestroyTexture(txs[i]);
			txs[i] = NULL;
		}
	}
}

enum font_id {
	FONT_SCORE,
	FONT_TEAM,
	FONT_STATUS,
	FONT_HINT,
	FONT_HINTS_ANSWER,
	FONT_HINT_TIMER_PTS,

	FONT_TOTAL
};
TTF_Font *fonts[FONT_TOTAL];

int load_fonts(void) {
	if (!(fonts[FONT_SCORE] = TTF_OpenFont("assets/OpenSans-SemiBold.ttf", 48))) {
		log_err("failed to load font");
		goto failed_font_score;
	}
	if (!(fonts[FONT_TEAM] = TTF_OpenFont("assets/OpenSans-SemiBold.ttf", 24))) {
		log_err("failed to load font");
		goto failed_font_team;
	}
	if (!(fonts[FONT_STATUS] = TTF_OpenFont("assets/OpenSans-SemiBold.ttf", 16))) {
		log_err("failed to load font");
		goto failed_font_status;
	}
	if (!(fonts[FONT_HINT] = TTF_OpenFont("assets/OpenSans-SemiBold.ttf", 18))) {
		log_err("failed to load font");
		goto failed_font_hint;
	}
	if (!(fonts[FONT_HINTS_ANSWER] = TTF_OpenFont("assets/OpenSans-SemiBold.ttf", 14))) {
		log_err("failed to load font");
		goto failed_font_hints_answer;
	}
	if (!(fonts[FONT_HINT_TIMER_PTS] = TTF_OpenFont("assets/OpenSans-SemiBold.ttf", 14))) {
		log_err("failed to load font");
		goto failed_font_hint_timer_pts;
	}
	return 0;
	TTF_CloseFont(fonts[FONT_HINT_TIMER_PTS]);
failed_font_hint_timer_pts:
	TTF_CloseFont(fonts[FONT_HINTS_ANSWER]);
failed_font_hints_answer:
	TTF_CloseFont(fonts[FONT_HINT]);
failed_font_hint:
	TTF_CloseFont(fonts[FONT_STATUS]);
failed_font_status:
	TTF_CloseFont(fonts[FONT_TEAM]);
failed_font_team:
	TTF_CloseFont(fonts[FONT_SCORE]);
failed_font_score:
	return 1;
}
void close_fonts(void) {
	for (enum font_id i = 0; i < FONT_TOTAL; ++i) {
		if (fonts[i]) {
			TTF_CloseFont(fonts[i]);
		} else {
			log_wtf("no font to close");
		}
	}
}

enum text_id {
	TEXT_TEAM_BLUE,
	TEXT_TEAM_RED,
	TEXT_SCORE_BLUE,
	TEXT_SCORE_RED,
	TEXT_STATUS,
	TEXT_HINT1,
	TEXT_HINT2,
	TEXT_HINT3,
	TEXT_HINT4,
	TEXT_HINTS_ANSWER,
	TEXT_HINT_TIMER_PTS,

	TEXT_TOTAL
};
SDL_Texture *text_txs[TEXT_TOTAL];

int render_text_tx(
		enum text_id text_id,                     // old texture is properly freed
		const char *text,                         // non-empty string to display
		enum font_id font_id, const SDL_Color fg, // font parameters
		int *w, int *h                            // texture dimensions (explained below)
		) {
	if (text_txs[text_id]) {
		SDL_DestroyTexture(text_txs[text_id]);
	}
	SDL_Surface *surf;
	if (!(surf = TTF_RenderUTF8_Blended(fonts[font_id], text, fg))) {
		log_err("failed to render surface (%s)", TTF_GetError());
		return 1;
	}
	// there doesn't seem to be a way to get
	// width and height from `SDL_Texture`,
	// which is why it's necessary to return
	// them from this function as `SDL_Surface`
	// is still accessible
	*w = surf->w, *h = surf->h;
	text_txs[text_id] = SDL_CreateTextureFromSurface(rend, surf);
	SDL_FreeSurface(surf);
	if (!text_txs[text_id]) {
		log_err("failed to create a texture from surface");
		return 1;
	}
	return 0;
}

